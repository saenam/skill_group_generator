import re
import pickle
import json
import numpy as np
import time
from gensim.models import Word2Vec
import sklearn
from sklearn.cluster import AgglomerativeClustering
import sys
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',
                    level=logging.INFO)
rootLogger = logging.getLogger()
rootLogger.setLevel(logging.INFO)


with open('./bigrammer.p', 'rb') as f:
    bigram = pickle.load(f)

with open('./trigrammer.p', 'rb') as f:
    trigram = pickle.load(f)


class text_token_iterator:

    def __init__(self, resume_path):
        self.resume_path = resume_path

    def __iter__(self):

        with open(self.resume_path) as infile:

            for line in infile:
                try:
                    text = line.split('|||')[0]
                    yield tokenize_doc(text)

                except ValueError as e:
                    print(e)
                    pass
        raise StopIteration()


def clean_text(text):

    text = re.sub(r"[\\\.,]", " ", text)
    text = re.sub(r"[^a-zA-Z0-9_\- ]", "", text)

    return text


def tokenize_doc(doc):

    cleaned_doc = clean_text(doc)
    tokens = trigram[bigram[cleaned_doc.split()]]

    return tokens


def cluster_word_vectors(wv_matrix, index_to_word, n_clusters):

    cluster_alg = AgglomerativeClustering(n_clusters=n_clusters,
                                          linkage='ward')
    cluster_alg.fit(wv_matrix)
    vocab_size = wv_matrix.shape[0]
    nodes = []

    for j, entry in enumerate(cluster_alg.children_):
        new_list = []
        children = None
        for i, item in enumerate(entry):
            if item < vocab_size:
                new_list.append(index_to_word[item])
            else:
                idx = int(item - vocab_size)
                if children is not None:
                    children.append(idx)
                else:
                    children = [idx]
                new_list.extend(nodes[idx]["contents"])

        nodes.append({"id": j,
                      "contents": new_list,
                      "children": children,
                      "name": None})

    return nodes


def filter_word_vectors(wv_matrix, standard_skills, index_to_word):

    reduced_indicies = []
    reduced_index2word = []
    for i, entry in enumerate(index_to_word):
        if entry in standard_skills:
            reduced_index2word.append(entry)
            reduced_indicies.append(i)

    reduced_indicies = np.array(reduced_indicies)
    filtered_wv = wv_matrix[reduced_indicies]

    return reduced_index2word, filtered_wv


if __name__ == '__main__':

    if len(sys.argv) > 1:
        resume_path = sys.argv[1]
    else:
        resume_path = './example.csv'

    rootLogger.info("*** Training word vectors using "+resume_path+" ***")
    text_iterator = text_token_iterator(resume_path)
    model = Word2Vec(text_iterator, size=300, window=5,
                     min_count=5, workers=8)
    index_to_word, wv_matrix = model.wv.index2word, model.wv.syn0
    rootLogger.info("*** Finished training Word Vectors ***")

    if len(sys.argv) > 2:
        standard_skills_path = sys.argv[2]
    else:
        standard_skills_path = './standard_skills.p'

    with open(standard_skills_path, 'rb') as f:
        standard_skills = pickle.load(f)

    index_to_word, wv_matrix = filter_word_vectors(wv_matrix,
                                                   standard_skills,
                                                   index_to_word)
    rootLogger.info("*** Clustering skills ***")
    nodes = cluster_word_vectors(wv_matrix, index_to_word, n_clusters=1)
    rootLogger.info("*** Finished clustering skills ***")

    current_time = '_'.join(time.ctime().split())
    model.save('word2vec_model_run_'+current_time)
    node_savepath = 'nodes_run_'+current_time+'.json'
    rootLogger.info("*** Saving skill groups as "+node_savepath+" ***")
    with open(node_savepath, 'w') as outfile:
        json.dump(nodes, outfile)
