# README #

## Skill Group Generator ##

This repo is for a script that generates groups of related or similar skills.

# Prerequisites #

- Full list of packages in the requirements.txt
- Python 3+
- Fast BLAS (Atlas, OpenBLAS etc.)

# Running #

To run the script simply run the two commands in the root directory:

- `pip install -r requirements.txt`
- `python generate_skill_groups.py example.csv standard_skills.p`

The outputs of the script are a json file that contains the skill groups and a Word2Vec model.

For details about the script and documentation consult the tutorial notebook
